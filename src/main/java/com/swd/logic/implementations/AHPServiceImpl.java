package com.swd.logic.implementations;

import com.swd.logic.AHPService;
import com.swd.logic.models.AHPMatrix;
import com.swd.logic.models.Variant;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service("aHPService")
public class AHPServiceImpl implements AHPService {
    private void caluclateRankingValues(Variant criteriaMatrix, List<Variant> ofertMatrixs)
    {
        double[] sVectorFromPreferenceMatrix = criteriaMatrix.getMatrix().getSVector();
        double[] rank = new double[ofertMatrixs.get(0).getMatrix().getMatrix().getColumnDimension()];
        double result;
        for (int j = 0; j < rank.length; j++) {
            result = 0.0;
            for (int i = 0; i < ofertMatrixs.size(); i++) {
                result += sVectorFromPreferenceMatrix[i] * ofertMatrixs.get(i).getMatrix().getSVector()[j];
            }
            rank[j] = result;
        }

        for (int i = 0; i < ofertMatrixs.size(); i++) {
            if (i < rank.length) {
                ofertMatrixs.get(i).setRankValue(rank[i]);
            }
        }
    }

    public List<Variant> getRanking(Variant criteriaMatrix, List<Variant> ofertMatrixs) {
        caluclateRankingValues(criteriaMatrix, ofertMatrixs);
        Collections.sort(ofertMatrixs);
        return ofertMatrixs;
    }

    @Override
    public Variant buildVariant(String name, List<Double> userMatrix) {
        if (userMatrix == null || userMatrix.isEmpty())
            throw new RuntimeException("AHPService ERROR: userMatrix is null or empty!");

        double[][] createdMatrix = null;
        boolean endLocalLoop = false;
        int matrixSize = userMatrix.size();
        for (int i = 2; !endLocalLoop && i < matrixSize + 2; i++) {
            if (((i * i) - i) / 2 == matrixSize) {
                createdMatrix = new double[i][i];
                endLocalLoop = true;
            }
        }
        if (!endLocalLoop)
            throw new RuntimeException("AHPService ERROR: Something wrong when designate createdMatrix size. ");


        int lastElementIndex = 0;
        for (int i = 0; i < createdMatrix.length; i++) {
            for (int j = 0; j < createdMatrix.length; j++) {
                if (i == j) createdMatrix[i][j] = 1;
                else if (j > i) {
                    createdMatrix[i][j] = userMatrix.get(lastElementIndex);
                    lastElementIndex++;
                } else {
                    createdMatrix[i][j] = 1.0 / createdMatrix[j][i];
                }
            }
        }
        return new Variant(name, new AHPMatrix(createdMatrix));
    }
}
