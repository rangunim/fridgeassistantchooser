package com.swd.logic.models;

import Jama.Matrix;

import java.util.Arrays;

public class AHPMatrix {
    private Matrix matrix; //macierz "M(0)" lub kryterialna
    private Matrix normMatrix; //unormowana macierz "M(0)" lub kryterialna
    private double[] sVector; // wektor preferencji
    private double[] cVector;
    private double[] sVector2; // wektor preferencji
    private AHPMatrix corrected;

    public AHPMatrix(Matrix matrix) {
        if (matrix.getColumnDimension() != matrix.getRowDimension()) {
            throw new RuntimeException("Dimensions matrix doesn't equal");
        }
        this.matrix = matrix.copy();
        this.sVector = new double[matrix.getColumnDimension()];
        this.cVector = new double[matrix.getColumnDimension()];

        fillCVector();
        fillNormMatrix();
        fillSVector();
    }

    public AHPMatrix(double[][] matrixInArray) {
        this(new Matrix(matrixInArray));
    }

    private AHPMatrix(AHPMatrix matrix) {
        this.matrix = matrix.getMatrix().copy();
        this.normMatrix = matrix.getNormMatrix().copy();
        this.sVector = new double[matrix.sVector.length];
        this.cVector = new double[matrix.cVector.length];
        for (int i = 0; i < matrix.sVector.length; i++) {
            this.sVector[i] = matrix.sVector[i];
            this.cVector[i] = matrix.cVector[i];
        }
    }

    public AHPMatrix copy() {
        return new AHPMatrix(this);
    }

    public Matrix getMatrix() {
        return matrix;
    }

    public Matrix getNormMatrix() {
        return normMatrix;
    }

    public double[] getSVector() {
        return sVector;
    }

    public double[] getCVector() {
        return cVector;
    }


    public double[] getsVector2() {
        return sVector2;
    }

    public AHPMatrix getCorrected() {
        return corrected;
    }

    public boolean isCohesive() {
        double ri = 0;
        switch (matrix.getColumnDimension()) {
            case 2:
                ri = 0.001;
                break;
            case 3:
                ri = 0.52;
                break;
            case 4:
                ri = 0.89;
                break;
            case 5:
                ri = 1.11;
                break;
            default:
                throw new RuntimeException("RI VALUE ERROR");
        }

        double ci = 0;
        for (int i = 0; i < cVector.length; i++) {
            ci += cVector[i] * sVector[i];
        }
        ci = (ci - cVector.length) / (cVector.length - 1);
        //System.out.println("CI =" + ci + ", result (cr) =" + ci / ri);
        return ci / ri <= 0.1;
    }

    public void correctCohesive() { //TODO
        AHPMatrix copy = this.copy();
        double[] d = copy.getMatrix().eig().getRealEigenvalues();
        Arrays.sort(d);
        double max = d[d.length - 1];
        double[][] cM = copy.getMatrix().getArrayCopy();
        for (int i = 0; i < cM.length; i++) {
            for (int j = 0; j < cM.length; j++) {
                cM[i][j] /= max;
            }
        }
        /*int i=0;
        while(!copy.isCohesive() && Math.abs(sumValues(this.getSVector()) - sumValues(copy.getSVector())) <= 0.1 && i<11)
        {
           double[][] m =  copy.getMatrix().arrayTimes(copy.getMatrix()).transpose().getArrayCopy();
           copy = new AHPMatrix(this.name,m);
           i++;
        }*/
        corrected = new AHPMatrix(cM);
        matrix = corrected.getMatrix();
        fillCVector();
        fillNormMatrix();
        fillSVector();
    }

    private static double sumValues(double[] array) {
        double result = 0;
        for (double d : array) {
            result += d;
        }
        return result;
    }

    private void fillCVector() {
        double[][] matrixArray = matrix.getArray();
        for (int i = 0; i < matrixArray.length; i++) {
            for (int j = 0; j < matrixArray.length; j++) {
                cVector[i] += matrixArray[j][i];
            }
        }

    }

    private void fillNormMatrix() {
        double[][] matrixArray = matrix.getArray();
        double[][] result = new double[matrixArray.length][matrixArray.length];

        for (int i = 0; i < matrixArray.length; i++) {
            for (int j = 0; j < matrixArray.length; j++) {
                result[j][i] = matrixArray[j][i] / cVector[i];
            }
        }
        normMatrix = new Matrix(result);
    }

    private void fillSVector() {
        double[][] matrixArray = normMatrix.getArray();
        for (int i = 0; i < matrixArray.length; i++) {
            for (int j = 0; j < matrixArray.length; j++) {
                sVector[i] += matrixArray[i][j];
            }
            sVector[i] = sVector[i] / matrixArray.length;
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("AHPMatrix: \n");
        builder.append("matrix=[").append(Arrays.deepToString(matrix.getArray())).append("]\n");
        builder.append("normMatrix=[").append(Arrays.deepToString(normMatrix.getArray())).append("]\n");
        builder.append("sVector=").append(Arrays.toString(sVector)).append("\n");
        builder.append("cVector=").append(Arrays.toString(cVector)).append("\n");
        builder.append("This (AHP) matrix is").append(isCohesive() ? "" : " not").append(" cohesive").append("\n\n");
        return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AHPMatrix ahpMatrix = (AHPMatrix) o;

        if (!matrix.equals(ahpMatrix.matrix)) return false;
        if (!normMatrix.equals(ahpMatrix.normMatrix)) return false;
        if (!Arrays.equals(sVector, ahpMatrix.sVector)) return false;
        return Arrays.equals(cVector, ahpMatrix.cVector);
    }

    @Override
    public int hashCode() {
        int result = matrix.hashCode();
        result = 31 * result + normMatrix.hashCode();
        result = 31 * result + Arrays.hashCode(sVector);
        result = 31 * result + Arrays.hashCode(cVector);
        return result;
    }
}
