package com.swd.logic.models;


public class Variant implements Comparable<Variant> {
    private String name;
    private AHPMatrix matrix;
    private double rankValue;


    public Variant(String name, AHPMatrix matrix) {
        this.name = name;
        this.matrix = matrix;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AHPMatrix getMatrix() {
        return matrix;
    }

    public void setMatrix(AHPMatrix matrix) {
        this.matrix = matrix;
    }

    public double getRankValue() {
        return rankValue;
    }

    public void setRankValue(double rankValue) {
        this.rankValue = rankValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Variant variant = (Variant) o;

        if (Double.compare(variant.rankValue, rankValue) != 0) return false;
        if (!name.equals(variant.name)) return false;
        return matrix.equals(variant.matrix);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = name.hashCode();
        result = 31 * result + matrix.hashCode();
        temp = Double.doubleToLongBits(rankValue);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Variant{" +
                "name='" + name + '\'' +
                ", matrix=" + matrix +
                ", rankValue=" + rankValue +
                '}';
    }

    @Override
    public int compareTo(Variant o) {
        if (this.rankValue < o.rankValue) return 1;
        else return this.rankValue == o.rankValue ? 0 : -1;
    }


}
