package com.swd.logic;


import com.swd.logic.models.Variant;

import java.util.List;


public interface AHPService {
    Variant buildVariant(String name, List<Double> userMatrix);

    List<Variant> getRanking(Variant criteriaMatrix, List<Variant> ofertMatrixs);
}
