package com.swd.web.models;

import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

public class Stage1SimpleForm {
    private List<String> ofertNames;

    @Size(min = 2, message = "Wybrano za małą liczbę kryteriów")
    private List<String> criteriaNames;

    public Stage1SimpleForm() {
        ofertNames = new ArrayList<>(5);
        criteriaNames = new ArrayList<>(5);
    }

    public List<String> getOfertNames() {
        return ofertNames;
    }

    public void setOfertNames(List<String> ofertNames) {
        this.ofertNames = ofertNames;
    }

    public List<String> getCriteriaNames() {
        return criteriaNames;
    }

    public void setCriteriaNames(List<String> criteriaNames) {
        this.criteriaNames = criteriaNames;
    }
}
