package com.swd.web.models;

import java.util.*;

public class Stage2SimpleForm {
    private List<Double> criteriaMatrix;
    private List<List<Double>> ofertMatrixs; //TODO pomyslec jak to zrobic w thymeleaf
    private List<Double> ofertMatrix1;
    private List<Double> ofertMatrix2;
    private List<Double> ofertMatrix3;
    private List<Double> ofertMatrix4;
    private List<Double> ofertMatrix5;

    private boolean acceptUncheckCohesion;


    public Stage2SimpleForm() {
        criteriaMatrix = new ArrayList<>(5);
        ofertMatrixs = new ArrayList<>(5);
    }


    public List<Double> getCriteriaMatrix() {
        return criteriaMatrix;
    }

    public void setCriteriaMatrix(List<Double> criteriaMatrix) {
        this.criteriaMatrix = criteriaMatrix;
    }

    public List<List<Double>> getOfertMatrixs() {
        return ofertMatrixs;
    }

    public void setOfertMatrixs(List<List<Double>> ofertMatrixs) {
        this.ofertMatrixs = ofertMatrixs;
    }


    public List<Double> getOfertMatrix1() {
        return ofertMatrix1;
    }

    public void setOfertMatrix1(List<Double> ofertMatrix1) {
        this.ofertMatrix1 = ofertMatrix1;
    }

    public List<Double> getOfertMatrix2() {
        return ofertMatrix2;
    }

    public void setOfertMatrix2(List<Double> ofertMatrix2) {
        this.ofertMatrix2 = ofertMatrix2;
    }

    public List<Double> getOfertMatrix3() {
        return ofertMatrix3;
    }

    public void setOfertMatrix3(List<Double> ofertMatrix3) {
        this.ofertMatrix3 = ofertMatrix3;
    }

    public List<Double> getOfertMatrix4() {
        return ofertMatrix4;
    }

    public void setOfertMatrix4(List<Double> ofertMatrix4) {
        this.ofertMatrix4 = ofertMatrix4;
    }

    public List<Double> getOfertMatrix5() {
        return ofertMatrix5;
    }

    public void setOfertMatrix5(List<Double> ofertMatrix5) {
        this.ofertMatrix5 = ofertMatrix5;
    }

    public boolean isAcceptUncheckCohesion() {
        return acceptUncheckCohesion;
    }

    public void setAcceptUncheckCohesion(boolean acceptUncheckCohesion) {
        this.acceptUncheckCohesion = acceptUncheckCohesion;
    }

}
