package com.swd.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpSession;

@Controller
public class HomeController {
    @GetMapping({"", "/index"})
    public String index() {
        return "Home/index";
    }

    @GetMapping("/about")
    public String about() {
        return "Home/about";
    }


    @GetMapping({"stage1", "solver"})
    public String stage1(HttpSession session) {
        session.invalidate();
        return "redirect:/solver/stage1";
    }
}
