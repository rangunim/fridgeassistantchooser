package com.swd.web.controllers;

import com.swd.logic.AHPService;
import com.swd.logic.models.Variant;
import com.swd.web.models.Stage1SimpleForm;
import com.swd.web.models.Stage2SimpleForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@Controller
@RequestMapping("solver")
@SessionAttributes("stage1Form")
public class SolverController {

    private static List<String> criteriaNames = new ArrayList<>();
    private static Map<Double, String> sattyRanking = new TreeMap<>();

    static {
        criteriaNames.add("Kolor");
        criteriaNames.add("Cena");
        criteriaNames.add("Pojemność zamrażarki");
        criteriaNames.add("Pojemność lodówki");
        criteriaNames.add("Klasa energetyczna");

        sattyRanking.put(1.0, "równie dobra");
        sattyRanking.put(3.0, "nieznacznie lepsza");
        sattyRanking.put(5.0, "wyraźnie lepsza");
        sattyRanking.put(7.0, "zdecydowanie lepsza");
        sattyRanking.put(9.0, "bezwzględnie lepsza");
        sattyRanking.put(1.0 / 3.0, "nieznacznie gorsza");
        sattyRanking.put(1.0 / 5.0, "wyraźnie gorsza");
        sattyRanking.put(1.0 / 7.0, "zdecydowanie gorsza");
        sattyRanking.put(1.0 / 9.0, "bezwzględnie gorsza");
    }

    private AHPService ahpService;

    @Autowired
    public SolverController(AHPService ahpService) {
        this.ahpService = ahpService;
    }

    @GetMapping("/stage1")
    public String stage1(Model model) {
        Stage1SimpleForm form = new Stage1SimpleForm();
        model.addAttribute("stage1SimpleForm", form);
        model.addAttribute("criterias", criteriaNames);

        return "Calculate/stage1";
    }

    @PostMapping("/stage1")
    public String stage1(@ModelAttribute("stage1SimpleForm") @Valid Stage1SimpleForm formModel, BindingResult result, HttpSession session, Model model) {
        List<String> offertsNames = new ArrayList<>(5);
        for (String s : formModel.getOfertNames()) {
            if (s != null && s != "") {
                offertsNames.add(s);
            }
        }
        formModel.setOfertNames(offertsNames);

        if (formModel.getOfertNames().size() < 2) {
            result.addError(new ObjectError("ofertNames", "Podano za małą liczbę ofert"));
            model.addAttribute("ofertNamesError", "Podano za małą liczbę ofert");
        }
        if (result.hasErrors()) {
            formModel.getOfertNames().clear();
            model.addAttribute("stage1SimpleForm", formModel);
            model.addAttribute("criterias", criteriaNames);
            return "Calculate/stage1";
        }


        session.setAttribute("stage1Form", formModel);
        return "redirect:stage2";
    }

    @GetMapping("/stage2")
    public String stage2(@ModelAttribute("stage1Form") Stage1SimpleForm stage1FormModel, Model model) {
        Stage2SimpleForm form = new Stage2SimpleForm();

        model.addAttribute("stage2SimpleForm", form);
        model.addAttribute("sF", stage1FormModel);
        model.addAttribute("sattyRanking", sattyRanking);
        return "Calculate/stage2";
    }

    @PostMapping("/stage2")
    public String stage2(@ModelAttribute("stage2SimpleForm") @Valid Stage2SimpleForm formModel, BindingResult result, RedirectAttributes redirectAttributes, Model model, HttpSession session) {
        //showInfoAboutDataInStage2(formModel, null);
        if (!formModel.isAcceptUncheckCohesion()) {
            if (!ahpService.buildVariant("", formModel.getCriteriaMatrix()).getMatrix().isCohesive()) {
                model.addAttribute("cohesiveCriteriaMatrixWarning", "Wypełnione dane są niespójne (sprzeczne)!");
                result.addError(new ObjectError("criteriaMatrix", ""));
            }

            correctOffertMatrixesInStage2(formModel);
            List<String> cohesiveOffertMatrixWarnings = new ArrayList<>(5);
            for (int i = 0; i < formModel.getOfertMatrixs().size(); i++) {
                if (!ahpService.buildVariant("", formModel.getOfertMatrixs().get(i)).getMatrix().isCohesive()) {
                    cohesiveOffertMatrixWarnings.add("Wypełnione dane są niespójne (sprzeczne)!");
                    result.addError(new ObjectError("ofertMatrixs", ""));
                } else {
                    cohesiveOffertMatrixWarnings.add(null);
                }
            }
            model.addAttribute("cohesiveOffertMatrixWarnings", cohesiveOffertMatrixWarnings);
        }
        if (result.hasErrors()) {
            //   formModel.getOfertMatrixs().clear();
            model.addAttribute("stage2SimpleForm", formModel);
            model.addAttribute("sF", session.getAttribute("stage1Form"));
            model.addAttribute("sattyRanking", sattyRanking);
            return "Calculate/stage2";
        }

        redirectAttributes.addFlashAttribute("stage2Form", formModel);
        return "redirect:showRanking";
    }

    @GetMapping("/showRanking")
    public String showRanking(@ModelAttribute("stage2Form") Stage2SimpleForm stage2Data, @ModelAttribute("stage1Form") Stage1SimpleForm stage1Data, Model model, HttpSession session) {
        //showInfoAboutDataInStage2(stage2Data, stage1Data);

        correctOffertMatrixesInStage2(stage2Data);
        Variant criteriaVariant = ahpService.buildVariant("criteriaMatrix", stage2Data.getCriteriaMatrix());
        List<Variant> ofertVariants = new ArrayList<>(stage1Data.getOfertNames().size());
        for (int i = 0; i < stage2Data.getOfertMatrixs().size(); i++) {
            List<Double> userMatrix = stage2Data.getOfertMatrixs().get(i);
            if (userMatrix != null && !userMatrix.isEmpty()) {
                ofertVariants.add(ahpService.buildVariant(stage1Data.getOfertNames().get(i % stage1Data.getOfertNames().size()), stage2Data.getOfertMatrixs().get(i)));
            }
        }

        ofertVariants = ahpService.getRanking(criteriaVariant, ofertVariants);
        for (Variant v : ofertVariants) {
            v.setName(v.getName().toUpperCase());
        }
        //showVaraintsValues(ofertVariants);

        model.addAttribute("ranking", ofertVariants);
        return "Calculate/showRanking";
    }

    private void correctOffertMatrixesInStage2(Stage2SimpleForm stage2Data) {
        stage2Data.getOfertMatrixs().clear();
        for (int i = 0; i < 5; i++) {
            switch (i) {
                case 0:
                    if (stage2Data.getOfertMatrix1() != null && !stage2Data.getOfertMatrix1().isEmpty()) {
                        stage2Data.getOfertMatrixs().add(stage2Data.getOfertMatrix1());
                    }
                    break;
                case 1:
                    if (stage2Data.getOfertMatrix2() != null && !stage2Data.getOfertMatrix2().isEmpty()) {
                        stage2Data.getOfertMatrixs().add(stage2Data.getOfertMatrix2());
                    }
                    break;
                case 2:
                    if (stage2Data.getOfertMatrix3() != null && !stage2Data.getOfertMatrix3().isEmpty()) {
                        stage2Data.getOfertMatrixs().add(stage2Data.getOfertMatrix3());
                    }
                    break;
                case 3:
                    if (stage2Data.getOfertMatrix4() != null && !stage2Data.getOfertMatrix4().isEmpty()) {
                        stage2Data.getOfertMatrixs().add(stage2Data.getOfertMatrix4());
                    }
                    break;
                case 4:
                    if (stage2Data.getOfertMatrix5() != null && !stage2Data.getOfertMatrix5().isEmpty()) {
                        stage2Data.getOfertMatrixs().add(stage2Data.getOfertMatrix5());
                    }
                    break;
            }
        }
    }


    private void showList(String title, List<Double> list) {
        System.out.print(title);
        if (list != null && !list.isEmpty()) {
            list.forEach(x -> System.out.print(x + ","));
        }
        System.out.print("]\n");
    }

    private void showInfoAboutDataInStage2(Stage2SimpleForm formModel, Stage1SimpleForm stage1Form) {

        if (stage1Form != null) {
            System.out.println("STAGE1 DATA:");
            System.out.print("OFFERT_NAMES: [");
            stage1Form.getOfertNames().forEach(x -> System.out.print(x + ","));
            System.out.print("]\n");
            System.out.print("CRITERIAS_NAMES: [");
            stage1Form.getCriteriaNames().forEach(x -> System.out.print(x + ","));
            System.out.print("]\n");
        }

        if (formModel != null) {
            System.out.println("STAGE2 DATA:");
            showList("Macierz kryteriów: [", formModel.getCriteriaMatrix());
            showList("Macierz offert1: [", formModel.getOfertMatrix1());
            showList("Macierz offert2: [", formModel.getOfertMatrix2());
            showList("Macierz offert3: [", formModel.getOfertMatrix3());
            showList("Macierz offert4: [", formModel.getOfertMatrix4());
            showList("Macierz offert5: [", formModel.getOfertMatrix5());

            System.out.print("Macierz offert: ["); //lista list
            for (List<Double> m : formModel.getOfertMatrixs()) {
                System.out.print("Macierz: [");
                m.forEach(x -> System.out.print(x + ","));
                System.out.print("]\n");
            }
            System.out.print("]\n");
        }
    }

    private void showVaraintsValues(List<Variant> variants) {
        System.out.println("\n Ranking parametry:");
        variants.forEach(x -> System.out.println(x + "\n"));
    }

    private void showList(List<List<Double>> list) {
        System.out.print("Macierz offert: ["); //lista list
        for (List<Double> m : list) {
            System.out.print("Macierz: [");
            if (m != null) m.forEach(x -> System.out.print(x + ","));
            else System.out.print("empty");
            System.out.print("]\n");
        }
        System.out.print("]\n");
    }
}
