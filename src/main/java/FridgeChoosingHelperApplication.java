package com.swd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@ComponentScan("com.swd")
public class FridgeChoosingHelperApplication {

    public static void main(String[] args) {
        SpringApplication.run(FridgeChoosingHelperApplication.class, args);
    }
}
